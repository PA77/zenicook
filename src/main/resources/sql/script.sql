drop table if exists recipes;
create table recipes
(
id text primary key,
name text not null,
recipe_content text not null,
cooking_time int,
preparation_time int,
difficulty int,
portions int,
picture_url text
);

drop table if exists steps;
create table steps
(
id text primary key,
orders int,
description text,
recipe_id text,
FOREIGN KEY (recipe_id) references recipes (id)
);

drop table if exists ingredients;
create table ingredients
(
id text primary key,
name text
);

drop table if exists recipes_ingredients;
create table recipes_ingredients
(
id text,
quantity int,
unit text,
recipe_id text,
ingredient_id text,
foreign key (recipe_id) references recipes (id),
foreign key (ingredient_id) references ingredients (id)
)