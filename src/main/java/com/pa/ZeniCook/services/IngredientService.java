package com.pa.ZeniCook.services;

import com.pa.ZeniCook.controllers.representations.ingredient.CreationIngredientRepresentation;
import com.pa.ZeniCook.domains.Ingredient;
import com.pa.ZeniCook.repositories.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class IngredientService {

    private IngredientRepository ingredientRepository;
    private IdGenerator idGenerator;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository, IdGenerator idGenerator){
        this.ingredientRepository = ingredientRepository;
        this.idGenerator = idGenerator;
    }

    public List<Ingredient> getAllIngredients(){
        return (List<Ingredient>) this.ingredientRepository.findAll();
    }

    public Optional<Ingredient> getOneById(String id){
        return this.ingredientRepository.findById(id);
    }

    public Ingredient createNewIngredient(CreationIngredientRepresentation c) throws Exception {
        if (this.ingredientRepository.findByName(c.getName().toLowerCase()).isEmpty()){
        Ingredient i = new Ingredient(this.idGenerator.generateNewId(), c.getName());
        return this.ingredientRepository.save(i);
        } else {
            throw new Exception("Cet ingredient existe déjà");
        }
    }

    public boolean deleteIngredient(String id){
        if (getOneById(id).isPresent()){
            this.ingredientRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }


}

