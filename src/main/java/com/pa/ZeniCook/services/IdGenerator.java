package com.pa.ZeniCook.services;

import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.UUID;

@Component
public class IdGenerator {

    public String generateNewId() {
        return UUID.randomUUID().toString();
    }

}

