package com.pa.ZeniCook.services;

import com.pa.ZeniCook.controllers.representations.ingredient.CreationIngredientRepresentation;
import com.pa.ZeniCook.controllers.representations.recipe.CreationRecipeRepresentation;
import com.pa.ZeniCook.controllers.representations.step.CreationStepRepresentation;
import com.pa.ZeniCook.domains.Ingredient;
import com.pa.ZeniCook.domains.Recipe;
import com.pa.ZeniCook.domains.RecipeIngredient;
import com.pa.ZeniCook.domains.Step;
import com.pa.ZeniCook.domains.compositeKey.CompositeKeyMeasures;
import com.pa.ZeniCook.repositories.IngredientRepository;
import com.pa.ZeniCook.repositories.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Component
public class RecipeService {

    private RecipeRepository recipeRepository;
    private IdGenerator idGenerator;
    private IngredientRepository ingredientRepository;

    @Autowired
    public RecipeService(RecipeRepository recipeRepository, IdGenerator idGenerator, IngredientRepository ingredientRepository) {
        this.recipeRepository = recipeRepository;
        this.idGenerator = idGenerator;
        this.ingredientRepository = ingredientRepository;
    }

    public List<Recipe> getAllRecipes() {
        return (List<Recipe>) this.recipeRepository.findAll();
    }

    public Optional<Recipe> getOneById(String id) {
        return this.recipeRepository.findById(id);
    }

    @Transactional(rollbackOn = Exception.class)
    public Recipe createNewRecipe(CreationRecipeRepresentation r) {
        String recipeId = this.idGenerator.generateNewId();

        Recipe recipe = new Recipe(recipeId, r.getName(), r.getRecipeContent(), r.getCookingTime(),
                r.getPreparationTime(), r.getDifficulty(), r.getPortions(), r.getPictureUrl(), this.stepsGestion(r), this.IngredientGestion(r, recipeId));
        return this.recipeRepository.save(recipe);
    }


    public boolean deleteARecipe(String id) {
        if (getOneById(id).isPresent()) {
            this.recipeRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public void updateRecipe(String id, CreationRecipeRepresentation creationRep) {
        String name = creationRep.getName();
        String recipeContent = creationRep.getRecipeContent();
        int cookingTime = creationRep.getCookingTime();
        int preparationTime = creationRep.getPreparationTime();
        int difficulty = creationRep.getDifficulty();
        int proportion = creationRep.getPortions();
        String pictureUrl = creationRep.getPictureUrl();
        List<CreationStepRepresentation> stepList = creationRep.getSteps();
        List<CreationIngredientRepresentation> ingredientList = creationRep.getIngredients();

        Optional<Recipe> optionalRecipe = this.recipeRepository.findById(id);
        optionalRecipe.map(r -> {
            if (name != null) {
                r.setName(name);
            }
            if (recipeContent != null) {
                r.setRecipeContent(recipeContent);
            }
            if (pictureUrl != null) {
                r.setPictureUrl(pictureUrl);
            }
            if (cookingTime != 0) {
                r.setCookingTime(cookingTime);
            }
            if (preparationTime != 0) {
                r.setPreparationTime(preparationTime);
            }
            if (difficulty != 0) {
                r.setDifficulty(difficulty);
            }
            if (proportion != 0) {
                r.setPortions(proportion);
            }
            if (stepList.size() != 0) {
                    r.setStepList(this.stepsGestion(creationRep));
                }
                if (ingredientList.size() != 0) {
                    r.getIngredients().clear();
                    r.getIngredients().addAll(this.IngredientGestion(creationRep, id));
                }
            this.recipeRepository.save(r);
            return r;
        });

    }

    public Recipe getRandomRecipe(){
        List<Recipe> recipes = (List<Recipe>) this.recipeRepository.findAll();
        Random r = new Random();
        return recipes.get(r.nextInt(recipes.size()));
    }


    private List<RecipeIngredient> IngredientGestion(CreationRecipeRepresentation r, String recipeId) {
        List<RecipeIngredient> ingredients = new ArrayList<>();

        for (CreationIngredientRepresentation c : r.getIngredients()) {
            if (this.ingredientRepository.findByName(c.getName().toLowerCase()).isEmpty()) {
                Ingredient ingredient = new Ingredient(this.idGenerator.generateNewId(), (c.getName().toLowerCase()));
                this.ingredientRepository.save(ingredient);
                RecipeIngredient recipeIngredient = new RecipeIngredient(new CompositeKeyMeasures(ingredient.getId(),
                        recipeId), c.getQuantity(), c.getUnit(), ingredient);
                ingredients.add(recipeIngredient);
            } else {
                Ingredient ingredient = this.ingredientRepository.findByName(c.getName().toLowerCase()).get();
                RecipeIngredient recipeIngredient = new RecipeIngredient
                        (new CompositeKeyMeasures(this.ingredientRepository.findId(c.getName().toLowerCase()), recipeId), c.getQuantity(), c.getUnit(), ingredient);
                ingredients.add(recipeIngredient);
            }
        }
        return ingredients;
    }

    private List<Step> stepsGestion(CreationRecipeRepresentation r) {
        List<Step> steps = new ArrayList<>();

        for (CreationStepRepresentation s : r.getSteps()) {
            Step step = new Step(this.idGenerator.generateNewId(), s.getOrders(), s.getDescription());
            steps.add(step);
        }
        return steps;
    }

}
