package com.pa.ZeniCook.services;

import com.pa.ZeniCook.controllers.representations.step.CreationStepRepresentation;
import com.pa.ZeniCook.domains.Step;
import com.pa.ZeniCook.repositories.StepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class StepService {

    private StepRepository stepRepository;
    private IdGenerator idGenerator;

    @Autowired
    public StepService(StepRepository stepRepository, IdGenerator idGenerator){
        this.stepRepository = stepRepository;
        this.idGenerator = idGenerator;
    }

    public List<Step> getAllSteps(){
        return (List<Step>) this.stepRepository.findAll();
    }

    public Optional<Step> getOneById(String id){
        return this.stepRepository.findById(id);
    }

    public Step createNewStep(CreationStepRepresentation c){
        Step step = new Step(this.idGenerator.generateNewId(), c.getOrders(), c.getDescription());
        return this.stepRepository.save(step);
    }

    public boolean deleteStep(String id){
        if (getOneById(id).isPresent()){
            this.stepRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}

