package com.pa.ZeniCook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZeniCookApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZeniCookApplication.class, args);
	}

}
