package com.pa.ZeniCook.controllers.representations.step;

import com.pa.ZeniCook.controllers.representations.recipe.DeleteRepresentationResult;
import com.pa.ZeniCook.domains.Step;
import com.pa.ZeniCook.services.StepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/steps")
public class StepController {

    private StepService stepService;
    private StepRepresentationMapper mapper;

    @Autowired
    public StepController(StepService stepService, StepRepresentationMapper mapper){
        this.stepService = stepService;
        this.mapper = mapper;
    }

    @GetMapping
    List<NewStepRepresentation> getRecipeSteps(){
        return this.stepService.getAllSteps().stream()
                .map(this.mapper::mapToDisplayableStep)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<NewStepRepresentation>getOneStepById(@PathVariable("id") String id) {
        Optional<Step> optionalStep = this.stepService.getOneById(id);

        return optionalStep
                .map(this.mapper::mapToDisplayableStep)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    NewStepRepresentation createNewStep(@RequestBody CreationStepRepresentation body){
        Step step = this.stepService.createNewStep(body);
        return new NewStepRepresentation(step.getId(), step.getOrders(), step.getDescription());
    }

    @DeleteMapping("/{id}")
    public DeleteRepresentationResult deleteStepeById(@PathVariable(value = "id") String id){
        boolean result = this.stepService.deleteStep(id);
        return new DeleteRepresentationResult(result);
    }

}
