package com.pa.ZeniCook.controllers.representations.step;

public class NewStepRepresentation {

    private String id;
    private int orders;
    private String description;

    public NewStepRepresentation(String id, int orders, String description){
        this.id = id;
        this.orders = orders;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
