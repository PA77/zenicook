package com.pa.ZeniCook.controllers.representations.recipe;

public class DeleteRepresentationResult {
    boolean result = true;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public DeleteRepresentationResult(boolean result){
        this.result = result;
    }
}
