package com.pa.ZeniCook.controllers.representations.recipe;

import com.pa.ZeniCook.controllers.representations.recipe.NewRecipeRepresentation;
import com.pa.ZeniCook.controllers.representations.recipeingredient.NewRecipeIngredientRepresentation;
import com.pa.ZeniCook.domains.Ingredient;
import com.pa.ZeniCook.domains.Recipe;
import com.pa.ZeniCook.domains.RecipeIngredient;
import com.pa.ZeniCook.repositories.IngredientRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RecipeRepresentationMapper {
    private IngredientRepository ingredientRepository;

    public NewRecipeRepresentation mapToDisplayableRecipe(Recipe recipe) {
        List<NewRecipeIngredientRepresentation> newList = new ArrayList<>();
        List<RecipeIngredient> ingredientList = recipe.getIngredients();
        for (RecipeIngredient r: ingredientList) {
            NewRecipeIngredientRepresentation nrir = new NewRecipeIngredientRepresentation(
                    r.getIngredient().getId(), r.getIngredient().getName(), r.getQuantity(), r.getUnit());
            newList.add(nrir);
        }
        NewRecipeRepresentation result = new NewRecipeRepresentation(recipe.getId(), recipe.getName(), recipe.getRecipeContent(),
                recipe.getCookingTime(), recipe.getPreparationTime(), recipe.getDifficulty(), recipe.getPortions(), recipe.getPictureUrl(), recipe.getStepList(), newList);
/*        result.setId(recipe.getId());
        result.setName(recipe.getName());
        result.setRecipeContent(recipe.getRecipeContent());
        result.setCookingTime(recipe.getCookingTime());
        result.setPreparationTime(recipe.getPreparationTime());
        result.setDifficulty(recipe.getDifficulty());
        result.setPortions(recipe.getPortions());
        result.setPictureUrl(recipe.getPictureUrl());*/
        return result;
    }
}
