package com.pa.ZeniCook.controllers.representations.ingredient;

import com.pa.ZeniCook.domains.Ingredient;
import org.springframework.stereotype.Component;

@Component
public class IngredientRepresentationMapper {
    public NewIngredientRepresentation mapToDisplayableIngredient(Ingredient ingredient){
        NewIngredientRepresentation newIR = new NewIngredientRepresentation(ingredient.getId(), ingredient.getName());
        return newIR;
    }
}
