package com.pa.ZeniCook.controllers.representations.ingredient;

import com.pa.ZeniCook.controllers.representations.recipe.DeleteRepresentationResult;
import com.pa.ZeniCook.domains.Ingredient;
import com.pa.ZeniCook.services.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ingredients")
public class IngredientController {

    private IngredientService ingredientService;
    private IngredientRepresentationMapper mapper;

    @Autowired
    public IngredientController(IngredientService ingredientService, IngredientRepresentationMapper mapper){
        this.ingredientService = ingredientService;
        this.mapper = mapper;
    }

    @GetMapping()
    public List<NewIngredientRepresentation>getAllIngredients(){
        return this.ingredientService.getAllIngredients().stream()
                .map(this.mapper::mapToDisplayableIngredient)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<NewIngredientRepresentation>getOneIngredient(@PathVariable("id") String id){
        Optional<Ingredient> ingredientOptional = this.ingredientService.getOneById(id);

        return ingredientOptional.map(this.mapper::mapToDisplayableIngredient)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public DeleteRepresentationResult deleteIngredient(@PathVariable("id") String id) {
        boolean result = this.ingredientService.deleteIngredient(id);
        return new DeleteRepresentationResult(result);
    }

    @PostMapping()
    public NewIngredientRepresentation createIngredient(@RequestBody CreationIngredientRepresentation body) throws Exception {
        Ingredient i = this.ingredientService.createNewIngredient(body);
        return new NewIngredientRepresentation(i.getId(), i.getName());
    }
}
