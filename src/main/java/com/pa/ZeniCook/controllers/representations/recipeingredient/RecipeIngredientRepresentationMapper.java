package com.pa.ZeniCook.controllers.representations.recipeingredient;

import com.pa.ZeniCook.controllers.representations.ingredient.NewIngredientRepresentation;
import com.pa.ZeniCook.domains.Ingredient;
import com.pa.ZeniCook.domains.RecipeIngredient;
import com.pa.ZeniCook.repositories.IngredientRepository;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@Component
public class RecipeIngredientRepresentationMapper {

    private IngredientRepository ingredientRepository;

    public NewRecipeIngredientRepresentation mapToDisplayableRecipeIngredient(RecipeIngredient rIP) {

        return new NewRecipeIngredientRepresentation(rIP.getIngredient().getId(), rIP.getIngredient().getName(),
                rIP.getQuantity(), rIP.getUnit());

    }

}
