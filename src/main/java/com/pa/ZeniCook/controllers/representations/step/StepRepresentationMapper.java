package com.pa.ZeniCook.controllers.representations.step;

import com.pa.ZeniCook.domains.Step;
import org.springframework.stereotype.Component;

@Component
public class StepRepresentationMapper {
    public NewStepRepresentation mapToDisplayableStep(Step step){
        NewStepRepresentation n = new NewStepRepresentation(step.getId(), step.getOrders(), step.getDescription());
        return n;
    }
}
