package com.pa.ZeniCook.controllers.representations.recipe;

import com.pa.ZeniCook.controllers.representations.recipe.CreationRecipeRepresentation;
import com.pa.ZeniCook.controllers.representations.recipe.DeleteRepresentationResult;
import com.pa.ZeniCook.controllers.representations.recipe.NewRecipeRepresentation;
import com.pa.ZeniCook.controllers.representations.recipe.RecipeRepresentationMapper;
import com.pa.ZeniCook.controllers.representations.recipeingredient.NewRecipeIngredientRepresentation;
import com.pa.ZeniCook.controllers.representations.recipeingredient.RecipeIngredientRepresentationMapper;
import com.pa.ZeniCook.domains.Recipe;
import com.pa.ZeniCook.domains.RecipeIngredient;
import com.pa.ZeniCook.services.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/recipes")
public class RecipeController {

    private RecipeService recipeService;
    private RecipeRepresentationMapper mapper;
    private RecipeIngredientRepresentationMapper mapper2;

    @Autowired
    public RecipeController(RecipeService recipeService, RecipeRepresentationMapper mapper, RecipeIngredientRepresentationMapper mapper2){
        this.recipeService = recipeService;
        this.mapper = mapper;
        this.mapper2 = mapper2;
    }

    @GetMapping
    List<NewRecipeRepresentation>getAllRecipes(){
        return this.recipeService.getAllRecipes().stream()
                .map(this.mapper::mapToDisplayableRecipe)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<NewRecipeRepresentation>getOneRecipe(@PathVariable("id") String id){
        Optional<Recipe> recipeOptional = this.recipeService.getOneById(id);

        return recipeOptional
                .map(this.mapper::mapToDisplayableRecipe)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public DeleteRepresentationResult deleteARecipeById(@PathVariable(value = "id") String id){
        boolean result = this.recipeService.deleteARecipe(id);
                return new DeleteRepresentationResult(result);
    }

    @PostMapping
    public NewRecipeRepresentation createRecipe(@RequestBody CreationRecipeRepresentation body){
            Recipe r = this.recipeService.createNewRecipe(body);
            List<RecipeIngredient> recipeIngredientList = r.getIngredients();
            List<NewRecipeIngredientRepresentation> newList = new ArrayList<>();
        for (RecipeIngredient recIng: recipeIngredientList) {
            NewRecipeIngredientRepresentation rep = this.mapper2.mapToDisplayableRecipeIngredient(recIng);
            newList.add(rep);
        }
        return new NewRecipeRepresentation(r.getId(), r.getName(), r.getRecipeContent(), r.getCookingTime(), r.getPreparationTime(),
                r.getDifficulty(), r.getPortions(), r.getPictureUrl(), r.getStepList(), newList);

    }

    @PutMapping("/{id}")
    public ResponseEntity<NewRecipeRepresentation> modifyRecipe(@PathVariable("id") String id, @RequestBody CreationRecipeRepresentation
            body) {
        this.recipeService.updateRecipe(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/random")
    public NewRecipeRepresentation sendToUserRandomRecipe(){
        return this.mapper.mapToDisplayableRecipe(this.recipeService.getRandomRecipe());
    }


}
