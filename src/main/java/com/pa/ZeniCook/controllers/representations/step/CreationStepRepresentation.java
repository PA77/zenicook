package com.pa.ZeniCook.controllers.representations.step;

public class CreationStepRepresentation {

    private int orders;
    private String description;

    public CreationStepRepresentation(int orders, String description){
        this.orders = orders;
        this.description = description;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
