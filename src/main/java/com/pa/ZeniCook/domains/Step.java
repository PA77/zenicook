package com.pa.ZeniCook.domains;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "steps")
public class Step implements Serializable {

    @Id
    private String id;
    private int orders;
    private String description;


    protected Step(){}

    public Step(String id, int order, String description) {
        this.id = id;
        this.orders = order;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
