package com.pa.ZeniCook.domains.compositeKey;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class CompositeKeyMeasures implements Serializable {

    @Column(name = "ingredient_id")
    private String ingredientId;

    @Column(name = "recipe_id")
    private String recipeId;

    protected CompositeKeyMeasures(){}

    public CompositeKeyMeasures(String ingredientId, String recipeId){
        this.ingredientId = ingredientId;
        this.recipeId = recipeId;
    }

    public String getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(String ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }
}
