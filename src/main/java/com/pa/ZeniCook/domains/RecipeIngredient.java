package com.pa.ZeniCook.domains;

import com.pa.ZeniCook.domains.compositeKey.CompositeKeyMeasures;

import javax.persistence.*;

@Entity
@Table(name = "recipes_ingredients")
public class RecipeIngredient {

    @EmbeddedId()
    private CompositeKeyMeasures id;

    @ManyToOne
    @JoinColumn(name = "recipe_id", updatable = false, insertable = false)
    private Recipe recipe;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ingredient_id", insertable = false, updatable = false)
    private Ingredient ingredient;



    private int quantity;
    private String unit;

    protected RecipeIngredient(){}

    public RecipeIngredient(CompositeKeyMeasures id, int quantity, String unit, Ingredient ingredient){
        this.quantity = quantity;
        this.unit = unit;
        this.id = id;
        this.ingredient = ingredient;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public CompositeKeyMeasures getId() {
        return id;
    }

    public void setId(CompositeKeyMeasures id) {
        this.id = id;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }
}
