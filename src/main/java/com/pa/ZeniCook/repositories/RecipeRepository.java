package com.pa.ZeniCook.repositories;

import com.pa.ZeniCook.domains.Recipe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeRepository extends CrudRepository<Recipe, String> {

}
