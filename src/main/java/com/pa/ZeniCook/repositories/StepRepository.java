package com.pa.ZeniCook.repositories;

import com.pa.ZeniCook.domains.Step;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StepRepository extends CrudRepository<Step, String> {
}
