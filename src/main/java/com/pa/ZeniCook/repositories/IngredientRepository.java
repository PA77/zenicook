package com.pa.ZeniCook.repositories;

import com.pa.ZeniCook.domains.Ingredient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IngredientRepository extends CrudRepository<Ingredient, String> {

   //@Query("select Ingredient from Ingredient where name = ?1")
   Optional<Ingredient> findByName(String name);

    @Query("select id from Ingredient where name = ?1")
    String findId(String name);
}
